CREATE TABLE auth (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL,
  ci VARCHAR(20) NOT NULL,
  username VARCHAR(50) NOT NULL,
  email VARCHAR(255) NOT NULL,
  rol ENUM('usuario', 'administrador', 'tecnico') NOT NULL,
  auth VARCHAR(255) NOT NULL
  
  );
