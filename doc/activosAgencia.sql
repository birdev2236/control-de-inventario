
--
-- Estructura de tabla para la tabla `tabla activos agencia`
--

CREATE TABLE activos_agencia (
id_activo INT PRIMARY KEY,
nombre_activo VARCHAR(255),
descripcion VARCHAR(255),
marca VARCHAR(255),
modelo VARCHAR(255),
num_serie VARCHAR(255),
costo_adquisicion DECIMAL(10,2),
estado VARCHAR(255),
fecha_adquisicion DATE,
fecha_ultima_revision DATE,
departamento_asignado VARCHAR(255),
ubicacion_fisica VARCHAR(255),
observaciones VARCHAR(255),
agencia_asignada VARCHAR(255)
);