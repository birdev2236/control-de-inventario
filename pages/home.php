<?php include("../components/header.php")?>
<?php include("../components/sidebar.php")?>



<div class="container-fluid">
<div class="row justify-content-left mt-4 ms-5">
    <div class="col-6 col-sm-4 col-md-3">
    <h1><strong>Inicio</strong></h>
    </div>
    </div>  
  <div class="row justify-content-center mt-4">
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../pages/home.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-home fa-5x mb-3"></i>
            <h5 class="card-title">Inicio</h5>
          </div>
        </div>
      </a>
    </div>
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../pages/inventario.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-laptop fa-5x mb-3"></i>
            <h5 class="card-title">Inventario</h5>
          </div>
        </div>
      </a>
    </div>
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../pages/usuario.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-users fa-5x mb-3"></i>
            <h5 class="card-title">Usuarios</h5>
          </div>
        </div>
      </a>
    </div>
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../formularios/activosForm.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-file-alt fa-5x mb-3"></i>
            <h5 class="card-title">Registro de Activos</h5>
          </div>
        </div>
      </a>
    </div>
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../formularios/agenciaForm.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-building fa-5x mb-3"></i>
            <h5 class="card-title">Registro de Agencias</h5>
          </div>
        </div>
      </a>
    </div>
    <div class="col-6 col-sm-4 col-md-3">
      <a href="../formularios/pages/usuariosForm.php" class="text-decoration-none">
        <div class="card text-center">
          <div class="card-body">
            <i class="fas fa-user-plus fa-5x mb-3"></i>
            <h5 class="card-title">Registro de Usuarios</h5>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>



<?php include("../components/footer.php")?>

