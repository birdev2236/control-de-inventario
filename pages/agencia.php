<?php 
  include("../config/conexion.php");
  include("../components/header.php");
  include("../components/sidebar.php");   
?>

<main class="container p-3">
  <div class="row">
    <div class="col-md-2">
      <!-- MESSAGES -->
      <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php session_unset(); } ?>
    </div>
    <div class="col-md-12 mx-auto">
      <div class="mb-3">
        <h2 class="text-center">Agencias</h2>
        <form action="search.php" method="get" class="form-inline justify-content-center custom-input4">
  <div class="form-group mx-sm-3 mb-2">
    <div class="row">
    <div class="col-sm-6">
    <input type="text" name="query" class="form-control" placeholder="Buscar...">
    </div>
    <div class="col-sm-4">
    <button type="submit" class="btn btn-primary mb-2">Buscar</button>
    </div>
  </div>
</form>
<div class="row">
    <div class="col-sm-8">
        <p>Generar Reporte</p>
        <a href="../config/generarExcel.php" class="btn btn-primary ml-2 float-right">
  <i class="fas fa-file-excel"></i>
</a>
        </div>
        </div>
      <table class="table text-center justify-content-center">
        <thead>
          <tr>
            <th scope="col" class="ce">Id</th>
            <th scope="col" class="ce">Nombre</th>
            <th scope="col" class="ce-2">Dirección</th>
            <th scope="col" class="ce">Teléfono</th>
            <th scope="col" class="ce">Correo</th>
            <th scope="col" class="ce">Fecha de Creación</th>
            <th scope="col" class="ce">Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $query = "SELECT * FROM agencias";
            $result_tasks = mysqli_query($conn, $query);    
            while($row = mysqli_fetch_assoc($result_tasks)) {
          ?>
          <tr>
            <td scope="row" class="table-light ce" ><?php echo $row['id']; ?></td>
            <td scope="row" class="table-light ce"><?php echo $row['nombre']; ?></td>
            <td scope="row" class="table-light ce"><?php echo $row['direccion']; ?></td>
            <td scope="row" class="table-light ce"><?php echo $row['telefono']; ?></td>
            <td scope="row" class="table-light ce"><?php echo $row['email']; ?></td>
            <td scope="row" class="table-light ce"><?php echo $row['fecha_creacion']; ?></td>
            <td scope="row" class="table-light ce"> <a href="edit.php?id=<?php echo $row['id']?>" class="btn btn-secondary "><i class="fas fa-marker"></i></td></a>
            <td scope="row" class="table-light ce"><a href="../config/delete.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i></td>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<?php include("../components/footer.php")?>
<script>
  $(document).ready(function() {
    $("#search-input").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".table tbody tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
</script>