<?php 
  include("../config/conexion.php");
  include("../components/header.php");
  include("../components/sidebar.php");   
?>

<main class="container p-3">
  <div class="row">
    <div class="col-md-2">
      <!-- MESSAGES -->
      <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php session_unset(); } ?>
    </div>
    <div class="col-md-12 mx-auto">
      <div class="mb-3">
        <h2 class="text-center">Inventario</h2>
        <form action="search.php" method="get" class="form-inline justify-content-center custom-input4">
  <div class="form-group mx-sm-3 mb-2">
    <div class="row">
    <div class="col-sm-6">
    <input type="text" id="search-input" name="query" class="form-control" placeholder="Buscar...">
    </div>
    <div class="col-sm-4">
    <button type="submit" class="btn btn-primary mb-2">Buscar</button>
    </div>
  </div>
</form>
<div class="row">
    <div class="col-sm-8">
        <p>Generar Reporte</p>
        <a href="../config/generarExcel.php" class="btn btn-primary ml-2 float-right">
  <i class="fas fa-file-excel"></i>
</a>
        </div>
        </div>
      <table class="table text-center justify-content-center">
      <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombre del Activo</th>
      <th scope="col">Descripción</th>
      <th scope="col">Marca</th>
      <th scope="col">Número de Serie</th>
      <th scope="col">Costo de Adquisición</th>
      <th scope="col">Estado</th>
      <th scope="col">Fecha de Adquisición</th>
      <th scope="col">Fecha Ultima Revisión</th>
      <th scope="col">Departamento Asignado</th>
      <th scope="col">Ubicación Física</th>
      <th scope="col">Observaciones</th>
      <th scope="col">Agencia Asignada</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $query = "SELECT * FROM activos_agencia";
      $result_tasks = mysqli_query($conn, $query);    
      while($row = mysqli_fetch_assoc($result_tasks)) {
    ?>
    <tr>
      <td scope="row" class="table-light"><?php echo $row['id_activo']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['nombre_activo']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['descripcion']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['marca']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['num_serie']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['costo_adquisicion']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['estado']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['fecha_adquisicion']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['fecha_ultima_revision']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['departamento_asignado']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['ubicacion_fisica']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['observaciones']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['agencia_asignada']; ?></td>
      <td scope="row" class="table-light">
        <a href="edit.php?id=<?php echo $row['id_activo']?>" class="btn btn-secondary">
          <i class="fas fa-marker"></i>
        </a>
        <a href="../config/delete.php?id=<?php echo $row['id_activo']?>" class="btn btn-danger">
          <i class="far fa-trash-alt"></i>
        </a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
    </div>
  </div>
</main>
<?php include("../components/footer.php")?>
<script>
  $(document).ready(function() {
    $("#search-input").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".table tbody tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
</script>
