<?php 
  include("../config/conexion.php");
  include("../components/header.php");
  include("../components/sidebar.php");   
?>

<main class="container p-3">
  <div class="row">
    <div class="col-md-2">
      <!-- MESSAGES -->
      <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php session_unset(); } ?>
    </div>
    <div class="col-md-12 mx-auto">
      <div class="mb-3">
        <h2 class="text-center">Usuarios del Sistema</h2>
        <form action="search.php" method="get" class="form-inline justify-content-center ">
  <div class="form-group mx-sm-3 mb-2">
    <div class="row">
    <div class="col-sm-6">
    <input type="text" id="search-input" name="query" class="form-control" placeholder="Buscar...">
    </div>
    <div class="col-sm-4">
    <button type="submit" class="btn btn-primary mb-2">Buscar</button>
    </div>
  </div>
</form>
<div class="row">
    <div class="col-sm-8">
        <p>Generar Reporte</p>
        <a href="../config/generarExcel.php" class="btn btn-primary ml-2 float-right">
  <i class="fas fa-file-excel"></i>
</a>
        </div>
        </div>
      <table class="table text-center justify-content-center">
      <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombres y Apellidos</th>
      <th scope="col">CI</th>
      <th scope="col">Nombre de Usuario</th>
      <th scope="col">Correo</th>
      <th scope="col">Rol de Usuario</th>
      <th scope="col">Metodo de Autenticación</th>
      <th scope="col">Id Cargo</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $query = "SELECT * FROM usuarios";
      $result_tasks = mysqli_query($conn, $query);    
      while($row = mysqli_fetch_assoc($result_tasks)) {
    ?>
    <tr>
      <td scope="row" class="table-light"><?php echo $row['id']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['nombre']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['ci']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['user']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['email']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['rol']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['auth']; ?></td>
      <td scope="row" class="table-light"><?php echo $row['id_cargo']; ?></td>
      <td scope="row" class="table-light">
        <a href="edit.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
          <i class="fas fa-marker"></i>
        </a>
        <a href="../config/delete.php?id=<?php echo $row['id']?>" class="btn btn-danger">
          <i class="far fa-trash-alt"></i>
        </a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
    </div>
  </div>
</main>
<?php include("../components/footer.php")?>
<script>
  $(document).ready(function() {
    $("#search-input").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".table tbody tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
</script>
