<?php 
    include("../config/conexion.php");
    include("../components/header.php");
    include("../components/sidebar.php");
?>
<?php 
    if(isset($_SESSION['message'])) {
?>
        <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
            <?= $_SESSION['message'] ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
<?php 
        session_unset(); 
    } 
?>
<form action="post.php" method="POST" style="margin-left: -100px;">
    <div class="container for-card">
        <div class="row justify-content-center">
            <div class="col-sm-10">
            <?php if(isset($_SESSION['message'])) { ?>
    <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message'] ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
                <div class="card card-sidebar">
                    <div class="card-header">
                        <h5>Registros de Agencia</h5>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row justify-content-center ">
                                <div class="col-sm-8"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Nombre de la Agencia</label>
                                        <input type="text" class="form-control custom-input3" name="nombre">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Dirección</label>
                                        <textarea class="form-control" name="direccion" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="row justify-content-center ">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Teléfono</label>
                                        <input type="text" class="form-control" name="telefono" placeholder="">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                </div>
                              </div>
                                <div class="row justify-content-center pbtn">
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="save_agency">Guardar</button>
                                    </div>
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="back">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include("../components/footer.php"); ?>
