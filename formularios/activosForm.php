<?php include("../config/conexion.php") ?>

<?php
// Realizamos la consulta SQL para obtener los nombres de las agencias
$query = "SELECT nombre FROM agencias";
$result = mysqli_query($conn, $query);
$options = '';
// Almacenamos los nombres de las agencias en la variable $options
while ($row = mysqli_fetch_array($result)) {
  $options .= '<option value="' . $row['nombre'] . '">' . $row['nombre'] . '</option>';
}
?>
<?php include("../components/header.php") ?>
<?php include("../components/sidebar.php") ?>

<form action="post.php" method="POST" style="margin-left: -100px;">
<div class="container for-card">
        <div class="row justify-content-center">
            <div class="col-sm-10">
            <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message'] ?>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
  <?php session_unset();
            } ?>
                <div class="card card-sidebar">
                    <div class="card-header">
                        <h5>Registros de Activos</h5>
                    </div>
                    <div class="card-body">
                        <div class="container">
                        <div class="row justify-content-center ">
  <div class="col-sm-8"> 
  <div class="form-group">
    <label for="formGroupExampleInput">Nombre Del Activo</label>
    <input type="text" class="form-control custom-input3" name="nombre_activo">
  </div>
  </div>
  </div>
  <div class="row justify-content-center ">
  <div class="col-sm-8"> 
  <div class="form-group">
  <label for="formGroupExampleInput">Descrición</label>
    <textarea class="form-control" name="descripcion" rows="2"></textarea>
  </div>
  </div>
  </div>
  <div class="row justify-content-center ">
  <div class="col-sm-4"> 
  <div class="form-group">
    <label for="formGroupExampleInput">Marca</label>
    <input type="text" class="form-control" name="modelo">
  </div>
  </div>
  <div class="col-sm-4">
  <div class="form-group">
    <label for="formGroupExampleInput">Número de Serie</label>
    <input type="text" class="form-control" name="num_serie">
 </div>
 </div>
 <div class="row justify-content-center ">
  <div class="col-sm-4"> 
 <div class="form-group">
    <label for="formGroupExampleInput">Costo de adquisición</label>
    <input type="text" class="form-control" name="costo_adquisicion">
  </div>
  </div>
 <div class="col-sm-4"> 
 <div class="form-group">
    <label for="formGroupExampleInput"> Estado</label>
    <input type="text" class="form-control" name="estado">
 </div>
 </div>
 </div>
 <div class="row justify-content-center ">
  <div class="col-sm-4"> 
 <div class="form-group">
    <label for="fecha_adquisicion">Fecha de adquisición</label>
    <input type="text" class="form-control flatpickr custom-input" name="fecha_adquisicion" placeholder="Fecha">
 </div>
 </div>
 <div class="col-sm-4 "> 
 <div class="input-group mb-3 pselect">
  <label class="input-group-text " for="department-select">Departamento</label>
  <select class="form-select" id="department-select" name="departmento">
    <option value="" selected disabled>Selecciona un departamento</option>
    <option value="ventas">Ventas</option>
    <option value="finanzas">Finanzas</option>
    <option value="recursos-humanos">Recursos Humanos</option>
    <option value="marketing">Marketing</option>
    <option value="produccion">Producción</option>
    <option value="programacion">Programación</option>
  </select>
</div>
  </div>
  </div>
  </div>
  </div>
  <div class="row justify-content-center ">
  <div class="col-sm-8"> 
  <div class="form-group">
  <label for="formGroupExampleInput">Ubicación Fisica</label>
    <input type="text" class="form-control custom-input3" name="ubicacion_fisica">
  </div>
  </div>
  </div>
  <div class="row justify-content-center ">
  <div class="col-sm-8"> 
  <div class="form-group">
  <label for="formGroupExampleInput">Observaciones</label>
  <textarea class="form-control" name="observaciones" rows="3"></textarea>
  </div>
  </div>
  </div>
  <div class="row justify-content-center ">
  <div class="col-sm-4"> 
  <div class="form-group">
    <label for="fecha_ultima_revision">Fecha de último mantenimiento</label>
    <input type="text" class="form-control flatpickr" name="fecha_ultima_revision" placeholder="Fecha ">
  </div>
  </div>
  <div class="col-sm-4"> 
 <div class="form-group">
    <label for="formGroupExampleInput">Agencia Asignada</label>
    <!-- Mostramos los nombres de las agencias en un select -->
    <select class="form-control" name="agencia_asignada">
      <?php echo $options ?>
    </select>
    </div>
    </div>
    </div>
    <div class="row justify-content-center pbtn">
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="save_activo">Guardar</button>
                                    </div>
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="back">Cancelar</button>
                                    </div>
                                </div>
</form>

<script>
  flatpickr(".flatpickr", {
    enableTime: false,
    dateFormat: "d-m-Y"
  });
</script>
<?php include("../components/footer.php") ?>

