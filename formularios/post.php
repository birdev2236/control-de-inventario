<?php
include("../config/conexion.php");

/*formulario agencia */

if (isset($_POST['save_agency'])){
    $nombre = $_POST['nombre'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['telefono'];
    $email = $_POST['email'];

    $query = "INSERT INTO agencias(nombre,direccion,telefono,email) VALUES ('$nombre','$direccion','$telefono','$email')";
   $result = mysqli_query($conn,$query);
if(!$result){
    die("Query Failed");
}
$_SESSION['message'] = 'Registro de Agencia realizado con exito';
$_SESSION['message_type'] = 'success';
header("Location: agenciaForm.php");
} 

if (isset($_POST['save_activo'])) {
    $nombre_activo = $_POST['nombre_activo'];
    $descripcion = $_POST['descripcion'];
    $modelo = $_POST['modelo'];
    $num_serie = $_POST['num_serie'];
    $costo_adquisicion = $_POST['costo_adquisicion'];
    $estado = $_POST['estado'];
    $fecha_adquisicion = $_POST['fecha_adquisicion'];
    $departamento_asignado = $_POST['departamento_asignado'];
    $ubicacion_fisica = $_POST['ubicacion_fisica'];
    $observaciones = $_POST['observaciones'];
    $fecha_ultima_revision = $_POST['fecha_ultima_revision'];
    $agencia_asignada = $_POST['agencia_asignada'];
  
    $query = "INSERT INTO activos_agencia (nombre_activo, descripcion, modelo, num_serie, costo_adquisicion, estado, fecha_adquisicion, departamento_asignado, ubicacion_fisica, observaciones, fecha_ultima_revision, agencia_asignada) VALUES ('$nombre_activo', '$descripcion', '$modelo', '$num_serie', '$costo_adquisicion', '$estado', '$fecha_adquisicion', '$departamento_asignado', '$ubicacion_fisica', '$observaciones', '$fecha_ultima_revision', '$agencia_asignada')";
    $result = mysqli_query($conn, $query);
    if (!$result) {
      die("Query Failed");
    }
    $_SESSION['message'] = 'Registro de Activo realizado con éxito';
    $_SESSION['message_type'] = 'success';
    header("Location: activosForm.php");
  }

  if (isset($_POST['save_user'])) {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $correo = $_POST['correo'];
    $fecha_nacimiento = $_POST['fecha_nacimiento'];
    $telefono = $_POST['telefono'];
    $ciudad = $_POST['ciudad'];
    $direccion = $_POST['direccion'];
    $codigo_postal = $_POST['codigo_postal'];
    $pais = $_POST['pais'];

    $query = "INSERT INTO usuarios (nombre, apellido, correo, telefono, fecha_nacimiento, ciudad, direccion, codigo_postal, pais) VALUES ('$nombre', '$apellido', '$correo','$telefono', '$fecha_nacimiento', '$ciudad', '$direccion', '$codigo_postal', '$pais')";
    $result = mysqli_query($conn, $query);
    if (!$result) {
        die("Query Failed");
    }
    $_SESSION['message'] = 'Registro de Usuario realizado con éxito';
    $_SESSION['message_type'] = 'success';
    header("Location: usuariosForm.php");
}

if(isset($_POST['save_auth'])) {
  $nombre = $_POST['nombre'];
  $ci = $_POST['ci'];
  $username = $_POST['username'];
  $email = $_POST['email'];
  $rol = $_POST['rol'];
  $id_cargo = $_POST['id_cargo'];
  $password = $_POST['password'];

  $hashed_password = password_hash($password, PASSWORD_DEFAULT);
   
  $query = "INSERT INTO auth (nombre, ci, user, email, rol, id_cargo, password) VALUES ('$nombre', '$ci', '$user', '$email', '$rol', '$id_cargo', '$hashed_password')";
$result = mysqli_query($conn, $query);

if (!$result) {
    die("Query Failed");
}

$_SESSION['message'] = 'Registro de Usuario en el sistema realizado con éxito';
$_SESSION['message_type'] = 'success';
header("Location: usuariosForm.php");

} 
?>