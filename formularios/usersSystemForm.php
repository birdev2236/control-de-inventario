<?php 
    include("../config/conexion.php");
    include("../components/header.php");
    include("../components/sidebar.php");
?>
<?php 
    if(isset($_SESSION['message'])) {
?>
        <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
            <?= $_SESSION['message'] ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
<?php 
        session_unset(); 
    } 
?>
<form action="post.php" method="POST" style="margin-left: -100px;">
    <div class="container for-card">
        <div class="row justify-content-center">
            <div class="col-sm-10">
            <?php if(isset($_SESSION['message'])) { ?>
    <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message'] ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
                <div class="card card-sidebar">
                    <div class="card-header">
                        <h5>Registros de Usuarios Del Sistema</h5>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row justify-content-center ">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Nombre y Apellido </label>
                                        <input type="text" class="form-control custom-input5" name="nombre">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">CI </label>
                                        <input type="text" class="form-control custom-input3" name="ci" placeholder>
                                </div>
                                </div>
                                </div>
                                <div class="row justify-content-center ">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Nombre de Usuario</label>
                                        <input type="text" class="form-control" name="user" >
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Correo</label>
                                        <input type="email" class="form-control" name="email" placeholder>
                                    </div>
                                </div>
                              </div>
                              <div class="row justify-content-center ">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">Rol</label>
                                    <select class="form-select" aria-label="Default select example" name="rol">
                                <option selected>Selecciona un Rol</option>
                                <option value="1">Usuario</option>
                                <option value="2">Administrador</option>
                                <option value="3">Técnico</option>
                                 </select>
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Id Cargo</label>
                                        <input type="text" class="form-control" name="id_cargo" >
                                    </div>
                                </div>
                              </div>
                              <div class="row justify-content-center ">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">Contraseña</label>
                                        <input type="password" class="form-control" name="password" >
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Confirmar Contraseña</label>
                                        <input type="password" class="form-control"  >
                                    </div>
                                </div>
                              </div>
                                <div class="row justify-content-center pbtn">
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="save_auth">Guardar</button>
                                    </div>
                                    <div class="col-sm-4 mb-2"> 
                                        <button class="btn btn-success w-100" name="back">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include("../components/footer.php"); ?>
