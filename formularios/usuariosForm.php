<?php include("../config/conexion.php") ?>
<?php include("../components/header.php")?>
<?php include("../components/sidebar.php")?>
<form action="post.php" method="POST" style="margin-left: -100px;">
    <div class="container for-card">
        <div class="row justify-content-center ">
        <div class="col-sm-10">
        <?php if(isset($_SESSION['message'])) { ?>
    <div class="alert alert-<?=$_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message'] ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
<?php session_unset(); } ?>
                <div class="card card-sidebar">
                    <div class="card-header">
                        <h5>Registros de Usuarios</h5>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Nombres</label>
                                        <input type="text" class="form-control" name="nombre">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Apellidos</label>
                                        <input type="text" class="form-control" name="apellido">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Correo</label>
                                        <input type="email" class="form-control" name="correo">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Teléfono</label>
                                        <input type="text" class="form-control" name="telefono">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Nombre de Usuario</label>
                                        <input type="username" class="form-control" name="usuario">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Contraseña</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Fecha de Nacimiento </label>
                                        <input type="text" class="form-control flatpickr" name="fecha_nacimiento" placeholder="Fecha de Nacimiento">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Ciudad</label>
                                        <input type="text" class="form-control" name="ciudad">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Codigo Postal </label>
                                        <input type="text" class="form-control  name="codigo_postal>
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Pais</label>
                                        <input type="text" class="form-control" name="pais">
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center pbtn">
                                <div class="col-sm-4 mb-2"> 
                                    <button class="btn btn-success w-100" name="save_user">Guardar</button>
                                </div>
                                <div class="col-sm-4 mb-2"> 
                                    <button class="btn btn-success w-100" name="back">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script>
  flatpickr(".flatpickr", {
    enableTime: false,
    dateFormat: "d-m-Y"
  });
</script>
<?php include("../components/footer.php")?>