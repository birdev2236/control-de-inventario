<?php 
  include("../config/conexion.php");
  include("../components/header.php");
?>
<?php
		// Define el archivo exportado
$arquivo = 'agencias.xls';

// Crea la tabla HTML
$html = '';
$html .= '<table border="1">';
$html .= '<tr>';
$html .= '<td colspan="7">Tabla de Agencias</tr>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td><b>ID</b></td>';
$html .= '<td><b>Nombre</b></td>';
$html .= '<td><b>Dirección</b></td>';
$html .= '<td><b>Teléfono</b></td>';
$html .= '<td><b>Correo</b></td>';
$html .= '<td><b>Fecha de Creación</b></td>';
$html .= '</tr>';

// Selecciona todos los elementos de la tabla "agencias"
$query = "SELECT * FROM agencias";
$result = mysqli_query($conn, $query);

while ($row = mysqli_fetch_assoc($result)) {
  $html .= '<tr>';
  $html .= '<td>' . $row["id"] . '</td>';
  $html .= '<td>' . $row["nombre"] . '</td>';
  $html .= '<td>' . $row["direccion"] . '</td>';
  $html .= '<td>' . $row["telefono"] . '</td>';
  $html .= '<td>' . $row["email"] . '</td>';
  $data = date('d/m/Y H:i:s', strtotime($row["fecha_creacion"]));
  $html .= '<td>' . $data . '</td>';
  $html .= '</tr>';
}

$html .= '</table>';

// Establece el encabezado para descargar el archivo
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename={$arquivo}");
echo $html;
exit();

?>

<?php include("../components/footer.php")?>