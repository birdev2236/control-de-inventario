<?php include("components/header.php")?>
<?php include("config/auth.php")?>

<?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>
<div class="wrapper">
        <div class="container main">
            <div class="row">
                <div class="col-md-6 side-image">
                    <!-------Image-------->
                    <img  src="img/loginInventario.jpg" alt="inventario">
                    <div class="text">
                    </div>
                </div>
                <div class="col-md-6 right">
                <form method="post" action="validar.php">
                     <div class="input-box">
                        <header>Control de Inventario</header>
                        <div class="input-field">
                            <input type="text" class="input" name="user" required autocomplete="off">
                            <label for="text">Usuario</label>
                        </div>
                        <div class="input-field">
                            <input type="password" class="input" name="password" required>
                            <label for="password">Contraseña</label>
                        </div>
                        <div class="input-field">
                            <input type="submit" class="submit" value="Iniciar Sesión">
                        </div>
                        <div class="signin">
                           <a href="#">¿Olvidaste tu cuenta?</a>
                        </div>
                        </form>
                     </div>
                </div>
            </div>
        </div>
    </div>
<?php include("components/footer.php")?>